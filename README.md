# Snowrite

<div align=right><em>winter, 2018</em></div>

[Open-source][2] mock of [Typora][1] for in-browser use.

## License

[The MIT License](LICENSE.txt).

[1]: https://typora.io "a closed-source markdown editor"
[2]: https://github.com/typora/typora-issues/issues/16
